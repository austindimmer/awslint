# CloudFormation Tests

Contains scripts for performing CloudFormation `test`s.

## Software Dependencies

* awscli
* [yamllint](https://github.com/adrienverge/yamllint)
* [jq](https://github.com/stedolan/jq)

## Scripts

Scripts are categorized with a type and a stage. The definitions are below:

### Types

* **Type:** CI: this script is primarily used to execute CI jobs by executing the script itself
* **Type:** Utility: this script is used by one or multiple `CI` or `Utility` scripts as a way to reuse and modularize code segments
* **Type:** CI/Utility: this script defines a function which can be used as a utility by other scripts and also has use cases as a standalone CI job

### Stages

* `unit test` - jobs designed to test the functionality of _individual_ scripts
* `qa` - jobs designed to ensure high-quality code in all non-functional aspects

### List of Scripts

#### [validate-json.sh](validate-json.sh)

Checks for JSON syntax errors in files with extension `.json` using `jq`.
**Type:** CI
**Stage:** `qa`

##### Required Parameters

None

##### Required Command Line Arguments

None

#### [validate-templates.sh](validate-templates.sh)

Performs the `validate-template` CloudFormation API action on files in an S3 bucket with the following extensions:

* template
* json
* yml
* yaml

Previous versions of this script performed `validate-template` against local files. However, a decision to check against S3-hosted files was driven by [limitations](https://docs.aws.amazon.com/AWSCloudFormation/latest/APIReference/API_ValidateTemplate.html#API_ValidateTemplate_RequestParameters) of `TemplateBody` over `TemplateURL` with regard to body length.
**Type:** CI
**Stage:** `unit test`

##### Required Parameters

None

##### Required Command Line Arguments

1. S3 bucket name
2. S3 key prefix

#### [validate-yaml.sh](validate-yaml.sh)

Uses `yamllint` to lint files with extensions `.yml` and `.yaml` for syntax errors.
**Type:** CI
**Stage:** `qa`

##### Required Parameters

None

##### Required Command Line Arguments

None
