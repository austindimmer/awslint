# GitLab CI Templates

This template is designed to be a complete CI/CD pipeline for `build`, `test`, and `deploy` of a CloudFormation stack.

## Stages

The basic flow of the pipeline is:

### 1. Build

* Create a temporary location in S3 to stage templates and other dependencies for use with the `TemplateURL` parameter during `test` stages.
* Cleanup any failed change sets by the same name from the CloudFormation stack of interest. This helps avoid conflicts when creating change sets later.

### 2. Unit Test

* Perform the AWS CloudFormation `validate-templates` API command to check the templates for any errors detected by the CloudFormation API.

### 3. Integration Test

* Create a "describe_only" change set. This change set evaluates the CloudFormation stack for changes determined by the new template, outputs them to the CI pipeline, and deletes the change set to avoid unintended execution. At the present time, any detected changes will cause the script to exit with an error code which is manifested in GitLab CI as a warning due to the `allow_failures: true` option.

### 4. Security Test

### 5. Quality Assurance (QA)

* Performs `yamllint` on yaml files with `.yml` or `.yaml` extensions. Runs on local files rather than the S3-hosted files.
* Performs JSON syntax error detection on any files with the `.json` extension. Runs on local files rather than the S3-hosted files.

### 6. Deliver

* Uploads CloudFormation templates and other deployment artifacts to the active S3 location for the environment.

### 7. Deploy

* Creates and executes a change set on the stack.

### 8. Cleanup

* Removes templates from the temporary S3 location

## Coming Soon

Steps not represented in this pipeline but planned for future implementation include:

* `cfn_nag` formal definition - currently a comment exists in the file
* stack drift detection as a `build` stage job
