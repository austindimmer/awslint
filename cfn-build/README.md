# cfn-build

CloudFormation utilities for the `build` phase of a CloudFormation CI/CD pipeline.

## Software Dependencies

1. awscli

## Scripts

Scripts are categorized with a type and a stage. The definitions are below:

### Types

* **Type:** CI: this script is primarily used to execute CI jobs by executing the script itself
* **Type:** Utility: this script is used by one or multiple `CI` or `Utility` scripts as a way to reuse and modularize code segments
* **Type:** CI/Utility: this script defines a function which can be used as a utility by other scripts and also has use cases as a standalone CI job

### Stages

### List of Scripts

#### [cleanup_bucket.sh](cleanup_bucket.sh)

Used to remove an S3 bucket and all objects contained within. Most useful for cleaning up buckets which were used as temporary staging locations for tests like `validate-template`.
**Type**: CI

##### Required Parameters

None

##### Required Command Line Arguments

1. the name of the S3 bucket to be deleted

#### [cleanup_failed_change_sets.sh](cleanup_failed_change_sets.sh)

Looks for change sets on the stack with the same stack name. If they exist and are in the failed state, we can safely delete them to proceed. If they are in any other state, we will preserve them to avoid conflicts.
**Type:** CI

##### Required Parameters

* CFN_STACK_NAME
* CFN_CHANGE_SET_NAME

#### [package_lambda_simple.sh](package_lambda_simple.sh)

Simple Lambda function packager; zips the entire contents of a directory. Recommended use as a build step with artifacts made available as GitLab CI artifacts.
**Type:** CI

##### Required Command Line Arguments

1. relative or absolute path of the parent folder under which Lambda script artifacts are contained

###### Directory Structure

```yml
build_root/
  lambda/folder/ <-- this is the path you pass to the script
    my-lambda-1
      myCode.js
    my-lambda-2
      myScript.py
```

###### Output

```yml
build_root/
  lambda/folder/
    my-lambda-1.zip
    my-lambda-2.zip
    my-lambda-1
      myCode.js
    my-lambda-2
      myScript.py
```

#### [stage_templates_s3.sh](stage_templates_s3.sh)

Uploads files to an S3 bucket based on the arguments passed. Creates the bucket if it does not exist.
**Type:** CI

##### Required Command Line Arguments

1. S3 bucket name for staging the files
2. Key prefix
3. A list of local paths to upload to S3

##### Recommended Uses

1. Stage S3 files in a temporary location to perform `validate-template` and evaluate describe-only change sets. This allows the use of the `--template-url` option instead of `--template-body` which has limitations as to file size.

```bash
./$CFN_BUILD_PATH/stage_templates_s3.sh $CI_PROJECT_NAME-temp-bucket $CFN_STAGING_S3_PREFIX cloudformation/myFile1.yaml cloudformation/myFile2.yaml
```

2. Stage S3 files in a permanent location after performing applicable testing.

```bash
./$CFN_BUILD_PATH/stage_templates_s3.sh $CFN_STAGING_BUCKET $CFN_STAGING_S3_PREFIX cloudformation/myFile1.yaml cloudformation/myFile2.yaml
```
