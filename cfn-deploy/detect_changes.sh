#!/bin/bash -e

detect_changes() {
  aws cloudformation describe-change-set \
    --stack-name $CFN_STACK_NAME \
    --change-set-name $CFN_CHANGE_SET_NAME > changeSetDetails.json
  cat changeSetDetails.json > /dev/stderr
  cat changeSetDetails.json | jq '.StatusReason' > statusReason.txt
  echo "Status reason: " | cat - statusReason.txt > /dev/stderr
  cat changeSetDetails.json | jq '.Changes' > changes.txt
  if grep "The submitted information didn't contain changes." statusReason.txt > /dev/null; then
    echo "Change set did not contain changes. Exiting..." > /dev/stderr
    exitCode=0
  elif [[ $(< changes.txt) == "[]" ]]; then
    echo "Change set did not contain changes. Exiting..." > /dev/stderr
    exitCode=0
  else
    echo "Change set contains changes. Review before deploying." > /dev/stderr
    exitCode=1 # exit with warning if allow_failure true or exit with failure otherwise
  fi
  echo "$exitCode"
}
