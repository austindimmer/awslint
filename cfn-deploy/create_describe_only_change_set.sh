#!/bin/bash

if [ -e cfn-deploy/delete_change_set.sh ]; then
  . cfn-deploy/delete_change_set.sh
else
  . /usr/bin/delete_change_set
fi

if [ -e cfn-deploy/create_change_set.sh ]; then
  ./cfn-deploy/create_change_set.sh
else
  create_change_set
fi

detect_changes_exit_code=$?
delete_change_set
exit $detect_changes_exit_code
